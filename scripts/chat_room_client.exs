Chat.Room.say(:fr, "Bob", "Bonjour Alice !")
Chat.Room.say(:en, "Mike", "Hello Joe?")
Chat.Room.say(:en, "Joe", "Hello Mike!")
Chat.Room.say(:fr, "Alice", "Salut Bob !")

Chat.Room.history(:fr) |> IO.inspect

Chat.Room.say(:fr, "Bob", "Tu viens à la boum ?")

Process.sleep(1000)

IO.puts("Room en:")
Chat.Room.history(:en) |> IO.inspect
Chat.Room.say(:en, "Robert", "Hello Joe?")

IO.puts("Room fr:")
Chat.Room.say(:fr, "Alice", "Bob, tout va bien ?")
Chat.Room.say(:fr, "Bob", "Oui, on disait quoi déjà ?")
Chat.Room.history(:fr) |> IO.inspect

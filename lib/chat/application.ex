defmodule Chat.Application do
  use Application

  def start(_type, _args) do
    Supervisor.start_link(
      [
        {Chat.Room, :fr},
        {Chat.Room, :en}
      ],
      strategy: :one_for_one,
      name: Chat.Supervisor
    )
  end
end

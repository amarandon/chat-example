defmodule Chat.Room do
  use GenServer
  import String, only: [contains?: 2]

  def start_link(name) do
    GenServer.start_link(__MODULE__, nil, name: name)
  end

  def say(pid, author, body) do
    GenServer.cast(pid, {:say, %{author: author, body: body}})
  end

  def history(pid) do
    GenServer.call(pid, :history)
  end

  def init(_), do: {:ok, []}

  def child_spec(name) do
    %{id: name, start: {__MODULE__, :start_link, [name]}}
  end

  def handle_cast({:say, message}, state) do
    if message.body |> contains?("boum") do
      raise "FATAL ERROR!!!!"
    end
    {:noreply, [message | state]}
  end

  def handle_call(:history, _, state) do
    {:reply, Enum.reverse(state), state}
  end
end
